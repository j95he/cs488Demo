 <br />
  <br />
   <br />
    <br />
     <br />
# <center> A5 Final Project: Escape!Yeah!<center>
### <center>Name: Jingwen He<center>
### <center>User ID: j95he<center>
### <center>Student ID: 20643357<center>

<div STYLE="page-break-after:always;">
</div>
# Contents
## 1 Introduction..........................................1
## 2 Primary Objective Details.........................2
## 3 Extra Objective Details
## 4 Bibliography

<div STYLE="page-break-after:always;">
</div>
# 1 Introduction
The purpose for taking this course is that I am interested in game industry. I am curious about how to make the picture of games looks like the real world and how to simulate the real world. I am interested in the VR area, and I think that graphics is an important problem that need to be solved in VR area in the future. Thus for the final project I choose the ray tracing part and I will try my best to simulate the real world in my scene in limited time. For each objective, I will search at first, and then convert the theory into my code. In order to

For detailed image show for each objective, you can visit https://git.uwaterloo.ca/j95he/cs488Demo

<div STYLE="page-break-after:always;">
</div>

## 2 Primary Objective
### (1.1)Extra primitive - Cone
![AA](hierCone.png)

### (1.2)Extra primitive - Cylinder
![AA-DOF](hier_cylinder.png)

### (2)Texture mapping
![AA-DOF](texture_mapping.png)

![AA-DOF](hier_earth1.png)

![AA-DOF](earth.png)

## Refraction
![AA-DOF](refraction.png)

## Glossy Refraction
![AA-DOF](glossy_refraction.png)

![AA-DOF](glossy_refraction2.png)

## Reflection
![AA-DOF](reflection.png)

## Glossy Reflection
![AA-DOF](glossy_reflection.png)

## Adaptive Antialiasing
![AA-DOF](aa_no.png)

![AA-DOF](aa.png)

![AA-DOF](aa_show.png)

## No Soft Shadow
![AA-DOF](soft_shadow_no.png)

## Soft Shadow with light number = 4x4
![AA-DOF](soft_shadow_1.png)

## Soft Shadow with light number = 8x8
![AA-DOF](soft_shadow_2.png)

## No Depth of Field
![AA-DOF](Depth_of_field_no.png)

## Depth of Field with small aperture
![AA-DOF](Depth_of_field_1.png)

## Depth of Field with larger aperture
![AA-DOF](Depth_of_field_2.png)

## Extra objective - Acceleration (kd tree)
I used kd tree for my mesh render. This also helps me a lot for render my final scene.
![AA-DOF](kd-tree.png)
![AA-DOF](macho-cows.png)

## Final Scene (Escape! Yeah!!!!!!!!)
Chopper tried to escape from the earth~
![AA-DOF](final_scene.png)

# CS488 Fall 2019 Project Code

---

## Dependencies
* OpenGL 3.2+
* GLFW
    * http://www.glfw.org/
* Lua
    * http://www.lua.org/
* Premake4
    * https://github.com/premake/premake-4.x/wiki
    * http://premake.github.io/download.html
* GLM
    * http://glm.g-truc.net/0.9.7/index.html
* ImGui
    * https://github.com/ocornut/imgui


---

## Building Projects
We use **premake4** as our cross-platform build system. First you will need to build all
the static libraries that the projects depend on. To build the libraries, open up a
terminal, and **cd** to the top level of the CS488 project directory and then run the
following:

    $ premake4 gmake
    $ make

This will build the following static libraries, and place them in the top level **lib**
folder of your cs488 project directory.
* libcs488-framework.a
* libglfw3.a
* libimgui.a

Next we can build a specific project.  To do this, **cd** into one of the project folders,
say **A0** for example, and run the following terminal commands in order to compile the A0 executable using all .cpp files in the A0 directory:

    $ cd A0/
    $ premake4 gmake
    $ make


----

## Windows
Sorry for all of the hardcore Microsoft fans out there.  We have not had time to test the build system on Windows yet. Currently our build steps work for OSX and Linux, but all the steps should be the same on Windows, except you will need different libraries to link against when building your project executables.  Some good news is that premake4 can output a Visual Studio .sln file by running:

    $ premake4 vs2013

 This should point you in the general direction.
