 <br />
  <br />
   <br />
    <br />
     <br />
      <div align=center>

# A5 Final Project: Escape!Yeah!
### Name: Jingwen He
### User ID: j95he
### Student ID: 20643357
</div>

<div STYLE="page-break-after:always;">
</div>

# Contents
## 1 Purpose List..........................................1
## 2 Introduction..........................................1
## 3 Technical Outline & Implementation.........................2
## 4 Extra Objective Details
## 5 Bibliography
## 6 Compilation
## 7 Manual
## 8 Objective List

<div STYLE="page-break-after:always;">
</div>

# 1 Purpose List
+ Render image to simulate the real world
    + Deeply understand how ray tracing work
    + What makes the image look real
+ Save the rum time of rendering to render good final scene with objectives as many as I can render together in the limited time.

<div STYLE="page-break-after:always;">
</div>

# 2 Introduction
The purpose for taking this course is that I am interested in game industry. I am curious about how to make the picture of games looks like the real world and how to simulate the real world. I am interested in the VR area, and I think that graphics is an important problem that need to be solved in VR area in the future. Thus for the final project I choose the ray tracing part and I will try my best to simulate the real world in my scene in limited time. For each objective, I will search at first, and then convert the theory into my code. In order to

**For online version of my report with more detailed image show for each objective, you can visit:** https://git.uwaterloo.ca/j95he/cs488Demo

<div STYLE="page-break-after:always;">
</div>

# 3 Technical Outline & Implementation
## 3.1 Extra primitive
**For cone:** I use the formula<sup>[1]</sup> to find the ray intersection with the cone which has a finite length on the positive y-axis where the peak point is at (0,0,0):
> $x^2 + z^2 = y^2$

And then use the conditions as showing<sup>[2]</sup>:

<div align=center>

<img src="cone_f.png" width = "200" />

</div>

> $(D.x^2+D.z^2-D.y^2)t^2 + 2(D.xE.x + D.zE.z - D.yE.y)t+ (E.x^2 +E.z^2 -E.y^2) = 0$  where D is the ray direction, E is the eye position<sup>[3]</sup>

Then use the $at^2+bt+c=0$, to get the roots $t1,t2$ where $t1 < t2$, and get the ray intersection position $p1,p2$. Then there are three conditions:
+  $p1<0$ and $0 \geq p2 \geq 1$: The intersection is at p2.
+ $0 \geq p1 \geq 1$: The intersection is at p1.
+ $p1>0$ and $0 \geq p2 \geq 1$: The intersection is at the top cap of the cone.

**For cylinder:** I use the formulas<sup>[4]</sup> to find the ray intersection with the cylinder which has a finite length on the y-axis where the center point is at (0,0,0):
> $x^2 + z^2 = 1$
> $(D.x^2+D.z^2)t^2 + 2(D.xE.x + D.zE.z)t+ (E.x^2 +E.z^2 -1) = 0$  where D is the ray direction, E is the eye position

Then use the $at^2+bt+c=0$, to get the roots $t1,t2$ where $t1 < t2$, and get the ray intersection position $p1,p2$. Then there are three conditions:
+  $p1<-1$ and $p2 \geq -1$: The intersection is at the bottom cap of the cylinder.
+ $1 \geq p1 \geq -1$: The intersection is at p1.
+ $p1>1$ and $1 \geq p2$: The intersection is at the top cap of the cylinder.

These two primitives help me saving time on rendering cones and cylinders instead of using mesh to show them.

<div STYLE="page-break-after:always;">
</div>

## 3.2 Texture mapping
For texture mapping, I use the formula to convert the intersection position on the sphere into the sphere UV-coordinate<sup>[5]</sup>:
> $u=0.5+arctan2(d.z+d.x)/2\pi$
> $v=0.5+arcsin(d.y)/\pi$

Then use the formula from the lecture:
```ruby
    float di = ((float)width - 1) * u;
    float dj = ((float)height - 1) * v;
    int i = (int) di;
    int j = (int) dj;
    float u_p = di - i;
    float v_p = dj - j;
    glm::vec3 color = get_pixel_color(i, j) * (1 - u_p) * (1 - v_p) +
                      get_pixel_color(i+1, j) * u_p * (1 - v_p); +
                      get_pixel_color(i, j+1) * (1 - u_p) * v_p +
                      get_pixel_color(i+1, j+1) * u_p * v_p;
```
We can get the pixel color from the texture image even the pixel position is not integer, the formula will work. I also add a new command "set_texture" in scene_lua.cpp to allow me to set the texture image for the object.

The texture mapping helps me to simulate the real texture of objects instead of only one color for each object.

<div align=center>

<img src="texture_mapping.png" width = "200" />

</div>

<div STYLE="page-break-after:always;">
</div>

## 3.3 Refraction
For the refraction, I added one argument, refract index, to my PHongmaterial class. I learned a lot from the book "*Fundamentals of Computer Graphics*". I followed these steps<sup>[6]</sup>:
```ruby
if (p is on a dielectric) then
  r = reflect(d, n )
  if (d · n < 0) then
    refract(d, n, n, t)
    c = −d · n
    kr =kg =kb =1
  else
    kr = exp(−art)
    kg = exp(−agt)
    kb = exp(−abt)
    if refract(d, −n, 1/n, t) then
      c=t·n else
      return k ∗ color(p + tr)
  R0 = (n−1)2/(n+1)2
  R=R0 +(1−R0)(1−c)5
  return k(R color(p + tr) + (1 − R) color(p + tt))
```
<div align=center>

<img src="refract_1.png" width = "200" />

</div>

+ $k$ is the value of how many light is blocked.
+ When $d · n < 0$, the light is from air to the object. When $d · n \geq 0$  the light is from object to the air.
+ $refract(d, −n, 1/n, t)$ function checks whether the light is total internal refraction. This use the formula:
  > Let $a = 1-[n^2(1-(d \cdot n)^2)]/n_t^2$
  >If $a \leq 0$, then it is total internal, and we just need to do the reflection.
  If this $a> 0$, then we do the refraction.

  The formula to compute refraction ray is:
  > $n(d-n(d\cdot n))/n_t - n\sqrt a$
+ $c$ is used for the  *Schlick approximation* which is approximation of the *Fresnel equations*. When there is a light ray pass from the object to the air, we use the $cosθ_t$ instead of $cosθ_i$ when $η_1 > η_2$ <sup>[7]</sup>. The value of $R$ is the approximation of the reflection ratio in the Fresnel equations. The value of $1-R$ is the approximation of the refraction ratio in the Fresnel equations.
+ I didn't create the perturb refraction ray because I need to check whether it is total internal refraction. So it is better to perturb the normal instead of refraction ray.
+ Then do the recursion to get the enough color to create the refraction effort.
<div align=center>
<img src="refraction.png" width = "200" />
</div>

## 3.4Glossy Refraction
For the Glossy refraction, I added one argument, glossy, to my PHongmaterial class. I learned a formula for creating the perturbation of normal of the intersection point on the object surface<sup>[8]</sup>
> 1. Create an orthonormal UVW-basis with $w = n$ where $n$ is the normal.
> 2. Create $u_p,v_p$:
  $u_p = -a/2 + ε a$
  $v_p = -a/2 + ε' a$
  where random $ε, ε' \in [0,1]$, $a$ is side length  centered at the origin
> 3. $n'=n+u_pu+v_pv$ where $n'$ is the perturb normal

With glossy refraction and refraction I can simulate any transparent and semi-transparent object. But these are time consuming depend on how how many recursion I took.

<div align=center>

<img src="glossy_refraction2.png" height = "200" />

</div>

<div STYLE="page-break-after:always;">
</div>

## 3.5 Reflection
For the reflection I used the formula from lecture:
> reflection ray direction = $d - 2n(d \cdot n)$ where $d$ is the direction of light ray, $n$ is the the normal

Then do the recursion to get the enough color to create the reflection effort.
<div align=center>
<img src="reflection.png" height = "200" />
</div>

## 3.6 Glossy Reflection
For the Glossy refraction, it is similar with glossy refraction. The only different is to create the perturbation of reflection ray instead of normal<sup>[8]</sup>:
> 1. Create an orthonormal UVW-basis with $w = r$ where $n$ is the relection ray.
> 2. Create $u_p,v_p$:
  $u_p = -a/2 + ε a$
  $v_p = -a/2 + ε' a$
  where random $ε, ε' \in [0,1]$, $a$ is side length  centered at the origin
> 3. $r'=r+u_pu+v_pv$ where $r'$ is the perturb reflection ray

With glossy reflection and reflection I can simulate any glass and rough glass. But these are time consuming depend on how how many recursion I took.

<div align=center>

<img src="glossy_reflection.png" height = "200" />

</div>

<div STYLE="page-break-after:always;">
</div>

## 3.7 Adaptive Antialiasing
For the Adaptive Antialiasing, I first render the picture without anialiasing once. Then I compare the color of eight neighbors(if exists) near each pixel.<sup>[10]</sup>
```ruby
int count = 1;
for (n: each neighbor of each pixel)
  d = abs(n.r - p.r) + abs(n.g - p.g) + abs(n.b - p.b)
  if (d > 0.1) then
    count++
    c += get color from random position between the n and p
p.color = (p.color + c)/count
```
Thus only the pixel with large color different with its neighbor pixels will do the antialiasing by get average color with specific neighbors instead of each neighbor to save the run time.
+ I test a lot of decide the threshold for taking antialiasing. I use the RGB calculator online to test how the number change will make significant different. Finally, I decided to take the sum of the absolute difference between each R,G,B color larger than $0.1$.

Adaptive Antialiasing helps to render clearer image. But this is time consuming if there are lots of pixels that get higher difference than the threshold. For example, if I choose a texture mapping of a  camouflage element.

<div align=center>

<img src="aa_show.png" height = "200" />

</div>

<div STYLE="page-break-after:always;">
</div>

## 3.8 Soft Shadow
For the soft shadow, I applied sampling in the part of checking shadow. If the ray between the intersection and the light position does not hit any object, then I get random sampling of the light position. <sup>[11]</sup>
```ruby
int row_number_light
for i from 0 to row_number_light
  for j from 0 to row_number_light
    new_light_pos.x = light_pos.x + i + a - 0.5;
    new_light_pos.y = light_pos.y + j + b - 0.5;
    check_shadow(new_light_pos)
    ...
```
where random $a,b \in [0,1]$

I have test many time with the different number of light. A 4x4 square light will make the shadow too rough. And finally I found that 8x8 square light looks perfect for my image. I did not choose to apply the sphere light because I though the square light is enough for me to simulate the soft shadow with a good result.

The soft shadow can simulate the real shadow in real world instead of the sharp shadow. But it is time consuming if the number of lights is very large.

<div align=center>

<img src="soft_shadow_2.png" height = "200" />

</div>

<div STYLE="page-break-after:always;">
</div>

## 3.9 Depth of Field
In the real pinhole camera, we have the lens to project light from the scene onto the image plane to control the depth of field. We follow these steps to apply the depth of field<sup>[8][12]</sup>:
> 1. Shoot the ray $R_1$ from the origin position for each pixel
> 2. Place image plane $s$ distance away, where I have the complete focus
> 3. Compute the intersection position $p$ of the $R_1$ and the image plane
> 4. The radius of the lens is $r$, randomly select $n$ points within a disk around the camera, where the disk is perpendicular to the camera view direction. The larger $r$ is, the rougher objects we get which are not place at the focus plane
> 5. Use those $n$ points as your eye position and shoot rays to the position $p$
> 6. Average the $n$ colors from those rays and assign it to the pixel

The depth of field is important when we take photos. It helps to show the distance relationship between the objects. But it is very time consuming, since all the pixel need to compute color with multiple times. The more random eye position is picked, the more time need to take.

<div align=center>

<img src="Depth_of_field_2.png" height = "200" />

</div>

<div STYLE="page-break-after:always;">
</div>

## 3.10 Final Scene
The name of my final scene called "**_Escape! Yeah!!!!!!!!_**".

In the final scene, Chopper is trying to escape from the earth because the earth will disappear in the next second. However Chopper does not realize that there is a glass cylinder under his foot. Thus he will fall into the water in the next seconds. The tree is last one on the earth.

I have applied:
+ A mesh of Chopper
    + different parts are different meshes
+ A sphere with texture mapping of earth
+ A mesh of tree
+ A smstdodeca mesh with reflection
+ A cuboid by using the cube with glossy Refraction
+ A mesh of ocean with reflection
+ Soft shadow applied
+ The background color is in gradient color to simulate the dusk, and get random white point to simulate the star:
  ```ruby
  float t = dir.y;
  final_color += (1.0 - t) * vec3(0.6, 0.2, 0.4) + t * vec3(0.0, 0.74, 1.0);
  if (a * (1 - t) < 0.001) then
          final_color += vec3(1);
  ```
  where $dir$ is the direction of light, random $a \in [0,1]$

I want to apply the depth of field as well, but I do not have enough time to render the image with both depth of field and soft shadow. At last I choose to apply the soft shadow in my final scene because the differences of depth of objects are not very large in my final, thus soft shadow seems more important to apply. The BVH in kd-tree structure helps me a lot in my final scene, since there are countless triangles in the meshes I used.

<div align=center>

<img src="final10.png" />

</div>

<div STYLE="page-break-after:always;">
</div>

# 4 Extra objective

## 4.1 Acceleration (Kd tree)
I used Kd-tree for my mesh render. This also helps me a lot for render my final scene.

The basic idea of bounding volume hierarchy is subdividing the bounding volume of a mesh for saving time of finding the intersection point between the light ray and the mesh.

I have add two fields, left and right, in my mesh class. For construct each mesh, I subdividing the mesh in to left and right part as two new meshes according to the sorted value of one of the axis (x, y, z) randomly  until the mesh has only one face. And when I check whether the ray has the intersection with the mesh, I check both left and right parts' bounding volumes. If one is not hit, then stop to traverse it.
And if we get one intersection from any parts of the mesh, we update the closest of the intersection point until we traverse all the possible parts.<sup>[13]</sup>

This is the data I collected from running the "*macho-cow.lua*" with only one thread. From the chart below, obviously, the one with Kd-tree runs at least three times quicker than the one without Kd-tree.

| image resolution | With Kd-tree | No Kd-tree |
| :--------------: | :----------: | :--------: |
| 256 x 256        |       3s     |     12s    |
| 512 x 512        |       15s    |     51s    |
| 1024 x 1024      |       62s    |     205s   |

<div align=center>

<img src="kd-tree.png" height = "300" />

</div>

<div align=center>

<img src="macho-cows.png" height = "200" />

</div>

<div STYLE="page-break-after:always;">
</div>

# 5 Reference

 [1] “Ray Tracing: Intersections with Cones.” Ray Tracing: Intersections with Cones - Everything2.Com, PortalCell, 26 Nov. 2011, everything2.com/title/Ray Tracing: intersections with cones.
 [2] Guertault, Julien. “Intersection of a Ray and a Cone.” Light Is Beautiful, 3 Jan. 2017, lousodrome.net/blog/light/2017/01/03/intersection-of-a-ray-and-a-cone/.
 [3] Ray Tracing Primitives, www.cl.cam.ac.uk/teaching/1999/AGraphHCI/SMAG/node2.html.
 [4] Penfold, Dom. “Cylinder Intersection.” woo4me, 29 Jan. 2014, woo4.me/wootracer/cylinder-intersection/.
 [5] “UV Mapping.” Wikipedia, Wikimedia Foundation, 30 Nov. 2018, en.wikipedia.org/wiki/UV_mapping.
 [6] Undefined, Undefined undefined. “Fundamentals of Computer Graphics.” Fundamentals of Computer Graphics, by Peter Shirley et al., 3rd ed., A K Peters, Natick, MA, 2009, pp. 304–307.
 [7] Greve, Bram de. “Reflections and Refractions in Ray Tracing.” (2006).
 [8] Shen, Hanwei. “Distributed Ray Tracing.” Ohio State University, web.cse.ohio-state.edu/~shen.94/681/Site/Slides_files/drt.pdf.
 [9] Shen, Hanwei. “Reflection and Refraction.” Ohio State University, web.cse.ohio-state.edu/~shen.94/681/Site/Slides_files/reflection_refraction.pdf.
 [10] Genetti, Jon D. and Dan Gordon. “Ray Tracing With Adaptive Supersampling in Object Space.” (2005).
 [11] Undefined, Undefined undefined. “Fundamentals of Computer Graphics.” Fundamentals of Computer Graphics, by Peter Shirley et al., 3rd ed., A K Peters, Natick, MA, 2009, pp. 311–313.
 [12] PENFOLD, DOM. “Depth of Field.” woo4me, 17AD, 2013, woo4.me/raytracer/depth-of-field/.
 [13] Bouatouch, Kadi. “Ray Tracing.” Institut De Recherche En Informatique Et Systèmes Aléatoires, Irisa, 13 Sept. 2005, www.irisa.fr/prive/kadi/Cours_LR2V/RayTracing.pdf.

 <div STYLE="page-break-after:always;">
 </div>

# 6 Compilation
+ Run the code with following step:

 		$ cd A5
  		 A5$ premake4 gmake
  		 A5$ make
		 A5$ cd Assets
  		 A5/Assets$ ../A5 puppet.lua
	  Make sure when you run "../A5", you are under the A5/Assets directory

+ Lab computer: gl30
# 7 Manual
+ gr.material( kd, ks, shiness, reflect_index, glossy, refract_index, k)
    + k : the value of absorb the light for transparent objects
    + reflect_index and refract_index cannot be both positive number
      + If one is positive, the another is 0
    + glossy is used for reflection or Refraction
    + refelct_index and glossy should be inputed together
    + refract_index and k should be inputed together
+ In A5.cpp there are some flags on the top:
  + multithread
  + soft Shadow
  + depth of field
  + antialiasing
+ In Mesh.cpp/hpp, there is a flag on the top:
  + BVH
+ When running the program, it will shows how long it has run and the percentages it has finished.


<div STYLE="page-break-after:always;">
</div>

# 8 Objective List
1. Extra Primitives(cylinder, cone)
2. Texture Mapping
3. Refraction
4. Glossy Refraction
5. Reflection
6. Glossy Reflection
7. Adaptive Antialiasing
8. Soft Shadow
9. Depth of Field
10. Final Scene
## Extra Objective List
1. Acceleration (Kd tree)
